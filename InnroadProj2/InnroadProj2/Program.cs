﻿using System;
using System.Collections.Generic;

namespace InnroadProj2
{
    class Program
    {
        static void Main(string[] args)
        {
            EmployeeDetails employee = new EmployeeDetails();
            VerifyOperation verify = new VerifyOperation();
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("Practise the collections in c#\nPress 1 to implement array\nPress 2 to implement list\nPress 3 to implement Dictionary\nPress any other character to exit");
                int selectedOption = Convert.ToInt32(Console.ReadLine());
                switch (selectedOption)
                {
                    case 1:
                        verify.VerifyDataStructure<EmployeeDetails[]>(employee.array);
                        break;
                    case 2:
                        verify.VerifyDataStructure<List<EmployeeDetails>>(employee.list);
                        break;
                    case 3:
                        verify.VerifyDataStructure<Dictionary<string,EmployeeDetails>>(employee.dictionary);
                        break;
                    default:
                        flag = false;
                        break;
                }
            }
        }
    }
}
