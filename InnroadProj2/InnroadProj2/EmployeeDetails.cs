﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnroadProj2
{
    public class EmployeeDetails
    {
        public EmployeeDetails[] array = new EmployeeDetails[10];
        public List<EmployeeDetails> list = new List<EmployeeDetails>();
        public Dictionary<string, EmployeeDetails> dictionary = new Dictionary<string, EmployeeDetails>();
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public decimal Salary { get; set; }
    }
}
