﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnroadProj2
{
    class ListOperations
    {
        EmployeeDetails emp = new EmployeeDetails();
        public void AddEmployeeInList()
        {
            Console.WriteLine("Please enter the firstname");
            string firstname = Console.ReadLine();
            Console.WriteLine("Please enter the lastname");
            string lastname = Console.ReadLine();
        start:
            Console.WriteLine("Please enter the email Id");
            string email = Console.ReadLine();
            foreach (var emp1 in emp.list)
            {
                if (emp1.Email == email)
                {
                    Console.WriteLine("Email already exists. Please enter new email");
                    goto start;
                }
            }
            Console.WriteLine("Please enter the salary");
            decimal salary = Convert.ToDecimal(Console.ReadLine());
            var employee = new EmployeeDetails()
            {
                Email = email,
                Firstname = firstname,
                Lastname = lastname,
                Salary = salary
            };
            emp.list.Add(employee);
            Console.WriteLine("Employee added in list");
        }
        public void DeleteEmployeeInList()
        {
            bool flag = false;
            Console.WriteLine("Enter the email id to delete");
            string email = Console.ReadLine();
            foreach (var emp1 in emp.list)
            {
                if (email == emp1.Email)
                {
                    emp.list.Remove(emp1);
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
            else
            {
                Console.WriteLine("Employee deleted successfully");
            }
        }
        public void DisplayUniqueEmployeeInList()
        {
            bool flag = false;
            Console.WriteLine("Enter the email id of the required Employee");
            string email = Console.ReadLine();
            foreach (var emp1 in emp.list)
            {
                if (email == emp1.Email)
                {
                    Console.WriteLine("Firstname= " + emp1.Firstname + " Lastname= " + emp1.Lastname + " Salary= " + emp1.Salary + " Email= " + emp1.Email);
                    flag = true;
                }
            }
            if (!flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
        }
        public void DisplayList()
        {
            bool flag = true;
            foreach (var emp1 in emp.list)
            {
                Console.WriteLine("Firstname= " + emp1.Firstname + " Lastname= " + emp1.Lastname + " Salary= " + emp1.Salary + " Email= " + emp1.Email);
                flag = false;
            }
            if (flag)
            {
                Console.WriteLine("No employees in Dictionary");
            }
        }
    }
}
