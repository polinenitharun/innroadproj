﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnroadProj2
{
    class DictionaryOperations
    {
        EmployeeDetails emp1 = new EmployeeDetails();
        public void AddEmployeeInDictionary()
        {
            Console.WriteLine("Please enter the firstname");
            string firstname = Console.ReadLine();
            Console.WriteLine("Please enter the lastname");
            string lastname = Console.ReadLine();
        start:
            Console.WriteLine("Please enter the email Id");
            string email = Console.ReadLine();
            foreach (var emp in emp1.dictionary.Keys)
            {
                if (emp == email)
                {
                    Console.WriteLine("Email already exists. Please enter new email");
                    goto start;
                }
            }
            Console.WriteLine("Please enter the salary");
            decimal salary = Convert.ToDecimal(Console.ReadLine());
            var employee = new EmployeeDetails()
            {
                Email = email,
                Firstname = firstname,
                Lastname = lastname,
                Salary = salary
            };
            emp1.dictionary.Add(email, employee);
            Console.WriteLine("employee added to dictionary");
        }
        public void DeleteEmployeeInDictionary()
        {
            bool flag = false;
            Console.WriteLine("Enter the email id to delete");
            string email = Console.ReadLine();
            foreach (var emp in emp1.dictionary)
            {
                if (email == emp.Key)
                {
                    emp1.dictionary.Remove(emp.Key);
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
            else
            {
                Console.WriteLine("Employee deleted successfully");
            }
        }
        public void DisplayUniqueEmployeeInDictionary()
        {
            bool flag = false;
            Console.WriteLine("Enter the email id of the required Employee");
            string email = Console.ReadLine();
            foreach (var emp in emp1.dictionary.Values)
            {
                if (email == emp.Email)
                {
                    Console.WriteLine("Firstname= " + emp.Firstname + " Lastname= " + emp.Lastname + " Salary= " + emp.Salary + " Email= " + emp.Email);
                    flag = true;
                }
            }
            if (!flag)
            {
                Console.WriteLine("Email Id does not exists. Please enter a valid email Id");
            }
        }
        public void DisplayDictionary()
        {
            bool flag = true;
            foreach (var emp in emp1.dictionary.Values)
            {
                Console.WriteLine("Firstname= " + emp.Firstname + " Lastname= " + emp.Lastname + " Salary= " + emp.Salary + " Email= " + emp.Email);
                flag = false;
            }
            if (flag)
            {
                Console.WriteLine("No employees in Dictionary");
            }
        }
    }
}
