﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InnroadProj2
{
    class VerifyOperation
    {
        ArrayOperations arr = new ArrayOperations();
        ListOperations lis = new ListOperations();
        DictionaryOperations dict = new DictionaryOperations();
        public void VerifyDataStructure<T>(T type)
        {
            Console.WriteLine(type);
            if (type.GetType().ToString().Equals("InnroadProj2.EmployeeDetails[]"))
            {
                bool flag = true;
                while (flag)
                {
                    DisplayOptions();
                    int selected = Convert.ToInt32(Console.ReadLine());
                    switch (selected)
                    {
                        case 1:
                            arr.AddEmployeeInArray();
                            break;
                        case 2:
                            arr.DeleteEmployeeInArray();
                            break;
                        case 3:
                            arr.DisplayUniqueEmployeeInArray();
                            break;
                        case 4:
                            arr.DisplayArray();
                            break;
                        default:
                            flag = false;
                            break;
                    }
                }
            }
            if (type.GetType().ToString().Equals("System.Collections.Generic.List`1[InnroadProj2.EmployeeDetails]"))
            {
                bool flag = true;
                while (flag)
                {
                    DisplayOptions();
                    int selected = Convert.ToInt32(Console.ReadLine());
                    switch (selected)
                    {
                        case 1:
                            lis.AddEmployeeInList();
                            break;
                        case 2:
                            lis.DeleteEmployeeInList();
                            break;
                        case 3:
                            lis.DisplayUniqueEmployeeInList();
                            break;
                        case 4:
                            lis.DisplayList();
                            break;
                        default:
                            flag = false;
                            break;
                    }
                }
            }
            if(type.GetType().ToString().Equals("System.Collections.Generic.Dictionary`2[System.String,InnroadProj2.EmployeeDetails]"))
            {
                bool flag = true;
                while (flag)
                {
                    DisplayOptions();
                    int selected = Convert.ToInt32(Console.ReadLine());
                    switch (selected)
                    {
                        case 1:
                            dict.AddEmployeeInDictionary();
                            break;
                        case 2:
                            dict.DeleteEmployeeInDictionary();
                            break;
                        case 3:
                            dict.DisplayUniqueEmployeeInDictionary();
                            break;
                        case 4:
                            dict.DisplayDictionary();
                            break;
                        default:
                            flag = false;
                            break;
                    }
                }
            }
        }
        public void DisplayOptions()
        {
            Console.WriteLine("Press 1 to Add an Employee\nPress 2 to delete an Employee\nPress 3 to Get Details of an Employee\nPress 4 to get details of all Employees\nPress any other character to exit");
        }
    }
}
