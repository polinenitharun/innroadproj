﻿namespace InnroadProj
{
    public interface EmployeeSchema
	{
        
	    string AddEmployee();
		void GetDetails();
		void UpdateEmployee();
	    void deleteEmployeeByEmail();
		void deleteEmployeeByName();
		void getUserByEmail();
		void TotalSalary();
	}
}
