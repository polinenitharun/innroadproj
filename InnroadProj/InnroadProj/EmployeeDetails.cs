﻿namespace InnroadProj
{
    public enum gender
    {
        male,
        female,
        others
    }
    public class EmployeeDetails
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string emailId { get; set; }
        public gender gender { get; set; }
        public int age { get; set; }
        public decimal salary { get; set; }

        public int employeeType { get; set; }
        
    }
}
