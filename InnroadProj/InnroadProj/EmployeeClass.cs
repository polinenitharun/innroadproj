﻿using System;
using System.Collections.Generic;

namespace InnroadProj
{
	//delegate declaration
	public delegate bool Check(string str);
	public delegate EmployeeDetails Check1(string s);
	public class EmployeeClass:EmployeeSchema
	{
		
		List<EmployeeDetails> emp = new List<EmployeeDetails>();
		//addition of employee to the list
		public string AddEmployee()
        {
			Console.WriteLine("please enter firstname");
			string _firstname = Console.ReadLine();
			if (!(_firstname.Length > 2 && _firstname.Length < 20))
			{
				Console.WriteLine("first name should be atleast 2 characters and not more than 20 characters\nPlease enter a valid name");
				AddEmployee();
			}
			Console.WriteLine("please enter lastname");
			string _lastname =Console.ReadLine();
			if (!(_lastname.Length > 2 && _lastname.Length < 20))
			{
				Console.WriteLine("last name should be atleast 2 characters and not more than 20 characters\nPlease enter a valid name");
				AddEmployee();
			}
			Console.WriteLine("please enter email address");
			string _emailId = Console.ReadLine();
			Check c = new Check(Perform);
			bool x=c(_emailId);
            if(x)
            {
				Console.WriteLine("User already exists\n Enter valid Email Id");
				display();
            }
			
			Console.WriteLine("please enter gender\nEnter 0 for male\nEnter 1 for female\nEnter 2 for other");
			gender gend = (gender)Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("please enter age");
			int _age = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine("please enter salary");
    		decimal _salary = Convert.ToDecimal(Console.ReadLine());
			Console.WriteLine("please enter Employee Type\nEnter 0 for PermanentEmployee\nEnter 1 for ContractualEmployee");
			int _employeeType = Convert.ToInt32(Console.ReadLine());
			var employee = new EmployeeDetails()
			{
				firstname = _firstname,
				lastname = _lastname,
				emailId = _emailId,
				gender = gend,
				age = _age,
				salary = _salary,
				employeeType = _employeeType
			};
			emp.Add(employee);			
			return "employee added";						
        }
		//delegate implementation
		public bool Perform(string str)
		{
			foreach (var employee in emp)
			{
				if (str == employee.emailId)
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			return false;
		}
		//delegate implementation
		public EmployeeDetails Perform1(string s)
        {
			for(int i = 0; i < emp.Count; i++)
            {
                if (s == emp[i].emailId)
                {
					return emp[i];
                }
               
            }
			return null;
        }
		//displaying details of all the employees
		public void GetDetails()
		{
			foreach(var employee in emp)
            {				
				string type = ContractCheck(employee.employeeType);
				Console.WriteLine(employee.firstname + " " + employee.lastname + " " + employee.emailId + " " + employee.gender + " " + employee.age + " " + employee.salary + " " + type);
            }	
		}
		//method overloading to display details of single employee

		public void GetDetails(string email)
		{
			bool fla = false;
			Check1 ck = new Check1(Perform1);
			EmployeeDetails employee = ck(email);
			if (!(object.ReferenceEquals(employee, null)))
			{
				string type = ContractCheck(employee.employeeType);
				Console.WriteLine(employee.firstname + " " + employee.lastname + " " + employee.emailId + " " + employee.gender + " " + employee.age + " " + employee.salary + " " + type);
				fla = true;
			}
			if (!fla)
			{
				Console.WriteLine("Employee Not Found");
			}
		}
        //updating details of employee
		public void UpdateEmployee()
		{
			Console.WriteLine("Please enter the employee email address");
			string email=Console.ReadLine();
			bool fla = false;
			Check1 ck = new Check1(Perform1);
			EmployeeDetails e = ck(email);
			if (!(object.ReferenceEquals(e, null)))
			{
				Console.WriteLine("Please enter the firstname to update, existing name is " + e.firstname);
					e.firstname = Console.ReadLine();
					Console.WriteLine("Please enter the lastname to update, existing name is " + e.lastname);
					e.lastname = Console.ReadLine();					
					Console.WriteLine("Please enter the gender to update, existing gender is " +e.gender);
					Console.WriteLine("please enter gender\nEnter 0 for male\nEnter 1 for female\nEnter 2 for other");
					e.gender = (gender)Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("Please enter the age to update, existing age is " + e.age);
					e.age = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("Please enter the salary to update, existing salary is " + e.salary);
					e.salary = Convert.ToDecimal(Console.ReadLine());
					string type = ContractCheck(e.employeeType);
					Console.WriteLine("Please enter the Employee Type to update, existing Type is " + type);
					Console.WriteLine("please enter Employee Type\nEnter 0 for PermanentEmployee\nEnter 1 for ContractualEmployee");
					e.employeeType = Convert.ToInt32(Console.ReadLine());
					fla = true;
				
            }
            if (fla)
            {
				Console.WriteLine("Employee Details Updated");
            }
            else
            {
				Console.WriteLine("Please enter a valid email address");
            }

		}
		//deleting employee details using name
		public void deleteEmployeeByName()
		{
			Console.WriteLine("Please enter the employee first name");
			string firstname = Console.ReadLine();
			bool fla = false;
			for(int i=0;i<emp.Count;i++)
			{
				if (firstname == emp[i].firstname)
				{
					emp.Remove(emp[i]);
					fla = true;
				}
			}
			if (fla)
			{
				Console.WriteLine("Employee Details Deleted");
			}
			else
			{
				Console.WriteLine("No Employee Details Present");
			}
		}
		//deleting employee details using email
		public void deleteEmployeeByEmail()
		{
			Console.WriteLine("Please enter the employee email address");
			string email=Console.ReadLine();
			bool fla = false;
			Check1 ck = new Check1(Perform1);
			EmployeeDetails employee = ck(email);
			if (!(object.ReferenceEquals(employee, null)))
			{
				emp.Remove(employee);
				fla = true;
			}
            if (fla)
            {
				Console.WriteLine("Employee Details Deleted");
            }
            else
            {
				Console.WriteLine("Please enter a valid email address");
            }
		}
		//displaying information of a specific user
		public void getUserByEmail()
		{
			Console.WriteLine("Please enter the email address of the required user");
			string email = Console.ReadLine();
			GetDetails(email);
		}
		public void TotalSalary()
        {
			Console.WriteLine("Please enter the email id");
			string email = Console.ReadLine();
			Check1 ck = new Check1(Perform1);
			EmployeeDetails e = ck(email);
			if (!(object.ReferenceEquals(e, null)))
			{
				if (e.employeeType == 0)
                {
					Console.WriteLine((12 * e.salary) + ((8 * e.salary) / 100));
                }
				else if (e.employeeType == 1)
                {
					Console.WriteLine((12 * e.salary));
                }   
            }
        }
		//method to check the employee type
		string ContractCheck(int e)
		{
			if (e == 0)
			{
				return "Permanent employee";
			}
			else if (e == 1)
			{
				return "Contractual employee";
			}
			else
			{
				return " ";
			}
		}
		void displayOptions()
		{
			Console.WriteLine("Please select the option\n");
			Console.WriteLine("Press 1 to add an employee\n");
			Console.WriteLine("Press 2 to get the details of all employees\n");
			Console.WriteLine("Press 3 to update an employee");
			Console.WriteLine("Press 4 to delete an employee by entering Employee email address\n");
			Console.WriteLine("Press 5 to delete employees by entering Employee name");
			Console.WriteLine("Press 6 to get details of specific employee by entering Employee address\n");
			Console.WriteLine("Press 7 to get Total salary of the employee\n");
			Console.WriteLine("Press any other key to exit");
		}
		
        public void display()
        {
			bool flag = true;
			while (flag)
			{
				displayOptions();
				int selectedOption = Convert.ToInt32(Console.ReadLine());
				switch(selectedOption)
				{
                    case 1:
						string msg=AddEmployee();
						Console.WriteLine(msg);
						break;
					case 2:
						GetDetails();
						break;
					case 3:
						UpdateEmployee();
						break;
					case 4:
						deleteEmployeeByEmail();
						break;
					case 5:
						deleteEmployeeByName();
						break;
					case 6:
						getUserByEmail();
						break;
					case 7:
						TotalSalary();
						break;
					default:
						flag = false;
						break;
				}
			}
		}

	}
}
	



		

